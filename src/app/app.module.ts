import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { FirebaseService } from './services/firebase.service';
import { UserService } from './services/user.service';
import { ChatService } from './services/chat.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { Ng2CarouselamosModule } from 'ng2-carouselamos';


import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatDetailComponent } from './components/chat/chat-detail/chat-detail.component';
import { ThreadsComponent } from './components/threads/threads.component';
import { ThreadComponent } from './components/thread/thread.component';
import { ProfileComponent } from './components/profile/profile.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';

import { MainPipe } from './services/main-pip.module';



export const firebaseConfig = {
  apiKey: "AIzaSyDxqxEoyb83QxVNvc93M3bHj8tzPTqs8WE",
  authDomain: "hfu-forum-webentwicklung.firebaseapp.com",
  databaseURL: "https://hfu-forum-webentwicklung.firebaseio.com",
  projectId: "hfu-forum-webentwicklung",
  storageBucket: "hfu-forum-webentwicklung.appspot.com",
  messagingSenderId: "481640040622"
}

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path: 'forum', component:ThreadsComponent},
  {path:'forum/:id', component:ThreadComponent},
  {path: 'profil', component:ProfileComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: DashboardComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    ThreadsComponent,
    ThreadComponent,
    ProfileComponent,
    NavigationComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    HomeComponent,
    ChatDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FlashMessagesModule,
    MainPipe,
    Ng2CarouselamosModule
  ],
  providers: [ UserService, FirebaseService, ChatService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
