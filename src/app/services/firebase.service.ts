import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { UserService } from './user.service';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class FirebaseService {
  threads: FirebaseListObservable<any>;
  thread: FirebaseObjectObservable<any>;
  recentThreads: FirebaseListObservable<any>;
  recentComments: FirebaseListObservable<any>;
  recentSubbedComments: FirebaseListObservable<any>;

  constructor(
    public db: AngularFireDatabase,
    public UserService: UserService,
    public afAuth: AngularFireAuth
  ) { }


  getThreads() {
    this.threads = this.db.list('/threads') as FirebaseListObservable<Thread[]>;
    return this.threads;
  }

  getThread(id: string) {
    this.thread = this.db.object('/threads/'+id) as FirebaseObjectObservable<Thread>;
    return this.thread;
  }

  addComment(thread$key, respondFormvalue, respondForm, thread) {
    console.log(respondFormvalue);
    console.log(respondFormvalue.text)
    this.afAuth.authState.subscribe(
      afUser => {
        this.db.object('/users/'+afUser.uid).subscribe(
          user => {
            let d = new Date();
            let answer = {
              date: d.toString(),
              from: user.$key,
              fromName: user.vorname + " " + user.name,
              text: respondFormvalue.text
            }

            this.db.list('/threads/'+thread$key+'/answers/').push(answer)


            let recentComment = {
              date: d.toString(),
              fromName: user.vorname + " " +user.name,
              text: respondFormvalue.text,
              threadTitle: thread.title,
              threadID: thread$key
            }
            this.db.list('/mostRecentComments/').push(recentComment)
              .then(
                _ => { respondForm.reset() }
              )
              .catch(error => { console.log(error) })
          }
        )
      }
    )
  }


  addThread(values, form) {
    this.afAuth.authState.subscribe(
      afUser => {
        this.db.object('/users/'+afUser.uid).subscribe(
          user => {
            let d = new Date();
            let newThread = {
              date: d.toString(),
              from: user.$key,
              category: values.category,
              title: values.title,
              fromName: user.vorname + " " + user.name,
              text: values.text
            }
            this.db.list('/threads/').push(newThread).then(snapshot => {
              this.db.list('/mostRecentThreads/').update(snapshot.key, newThread)
                .then(_ => {
                  form.reset();
                })
                .catch(error => { console.log(error)} )
            })
          }
        )
      }
    )
  }


  getRecentThreads() {
    this.recentThreads = this.db.list('/mostRecentThreads') as FirebaseListObservable<any>;
    return this.recentThreads;
  }

  getRecentComments() {
    this.recentComments = this.db.list('/mostRecentComments') as FirebaseListObservable<any>;
    return this.recentComments;
  }
}

interface Thread{
  answers?: {},
  category?: string,
  date: string,
  from: string,
  fromName: string,
  mostRecent?: {},
  text: string,
  title: string
}
