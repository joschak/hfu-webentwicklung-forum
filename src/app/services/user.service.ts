import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
declare var $:any;

@Injectable()
export class UserService {
  user: any;
  userFB: any;
  userID: any;
  subscriptions: any;
  subscription: any;
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFireDatabase,
    private router: Router,
    private flashMessage: FlashMessagesService
  ) { }


  loginGoogle() {
    this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(_ => {
      //Get User from own DB
      this.afAuth.authState.subscribe(user => {
        this.userFB = user;
        if(user) {
          this.db.object('/users/'+user.uid).subscribe(
            userDB => {
              if(userDB.$exists()) {//Check if Google User is already created in DB
                this.user = userDB;//save User (Observable) from own DB to UserService
                this.db.list('/chat/').update(this.user.$key, this.user);//Join Chat
              }else {//Google User has no Profile in DB
                // Create new User in DB
                let d = new Date();
                let newUser = {
                  name: " ",
                  vorname: user.displayName,
                  isGoogle: true,
                  registeredDate: d.toString(),
                  imgURL: user.photoURL,
                  chats: {}
                }
                this.db.list('/users').update(user.uid, newUser);
                this.db.list('/chat/').update(user.uid, newUser);
                //Login with Userdata from DB
                this.user = newUser;
              }
            },error => {
              console.log(error);
            }
          )
        }
        this.router.navigate(['/dashboard']);//login was succesfull -> navigate to dasboard

      }, error => console.log(error) )
    }).catch(error => {
      console.log(error);
    });
  }

  loginEmail(form, formElement) {
    //Login with Firebase User (Email)
    var successOrError;
    console.log(formElement);
    console.log(form.email);
    this.afAuth.auth.signInWithEmailAndPassword(form.email, form.password).then(_ => {
      //Get User from own DB
      this.afAuth.authState.subscribe(user => {
        this.userFB = user;
        if(user) {
          this.db.object('/users/'+user.uid).subscribe(
            userDB => {
              this.user = userDB;//save User (Observable) from own DB to UserService
              this.db.list('/chat/').update(this.user.$key, this.user);//Join Chat
            }, error => {
                this.errorMSG(error);
            }
          )
          this.router.navigate(['/dashboard']);
        }
      }, error => { this.errorMSG(error); } ), error => {this.errorMSG(error);}
      }).catch(error => {
        this.errorMSG(error);
      });
  }

  logout() {
    this.db.object('/chat/'+this.afAuth.auth.currentUser.uid).remove()
      .then(_ => {
        this.afAuth.auth.signOut()
          .then( _ => {
            console.log(this.afAuth.auth.currentUser);
            this.router.navigate(['/login']);
          })

      })
      .catch(error => { console.log(error)})

    this.user = null;
    this.userFB = null;



  }

  errorMSG(error) {
    console.log("errorMSGFUNC")
    $('#modalTitle').html(error.name);
    $('#modalText').html(error.message);
    $('#myModal').moda('show');
  }

  registerUser(userNew: User, form) {
    this.afAuth.auth.createUserWithEmailAndPassword(form.email, form.password).then(userFB => {
        this.userFB = userFB;
        this.afAuth.authState.subscribe(user => {
          this.db.list('/users').update(user.uid, userNew).then(user => {
            this.user = userNew;
            this.db.list('/chat/').update(this.user.$key, this.user);//Join Chat
          })
          this.router.navigate(['/dashboard']);
        });
    }).catch(error => {$('#myModal').modal('show')});
  }

  getUser() {
    this.afAuth.authState.subscribe(
      user => {
        if(user) {
          this.userID = user.uid;
        }

      }
    )
    try {
      this.user = this.db.object('/users/'+this.userID) as FirebaseObjectObservable<any>;
    }catch(error) {
      console.log(error);
    }

    return this.user;
  }


  getUserSubscriptions() {
    try {
      this.subscriptions = this.db.list('/users/'+this.afAuth.auth.currentUser.uid+'/subscriptions') as FirebaseListObservable<any>;
    }catch(error) {
      console.log(error)
    }

    return this.subscriptions;
  }

  getUserSubscription(id) {
    try {
        this.subscription = this.db.object('/users/'+this.afAuth.auth.currentUser.uid+'/subscriptions/'+id) as FirebaseObjectObservable<any>;
    }catch(error) {
      console.log(error);
    }

    return this.subscription;
  }

  subscribeThread(thread) {
    console.log("test");
    this.getUser().subscribe(
      user => {this.user = user}
    )
    let d = new Date();
    let sub = {
      date: d.toString()
    }
    try {
      this.db.list('/users/'+this.user.$key+'/subscriptions/'+thread.$key).update(thread.$key, sub);
    }catch(error) {
      console.log(error);
    }

  }

  unsubscribeThread(thread) {
    console.log("test");
    this.getUser().subscribe(
      user => {this.user = user}
    )
    try {
      this.db.object('/users/'+this.user.$key+'/subscriptions/'+thread.$key).remove();
    }catch(error) {
      console.log(error);
    }

  }

}


interface User {
  $key?:string;
  name?: string,
  vorname: string,
  imgURL?: string,
  registeredDate: string,
  chats?: {},
  subscriptions?: {},
  isGoogle: boolean
}
