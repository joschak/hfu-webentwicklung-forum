import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { UniquePipe } from './uniquePipe';

@NgModule({
  declarations:[UniquePipe],
  imports:[CommonModule],
  exports:[UniquePipe]
})

export class MainPipe{}
