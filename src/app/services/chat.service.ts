import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database';
import { UserService } from './user.service';
import { AngularFireAuth } from 'angularfire2/auth';


@Injectable()
export class ChatService {
  onlineUsers: any;
  messages: any;
  user: any;
  constructor(
    private db: AngularFireDatabase,
    private UserService: UserService,
    private afAuth: AngularFireAuth
  ) { }


  getOnlineUsers() {
    this.onlineUsers = this.db.list('/chat') as FirebaseListObservable<any>;
    return this.onlineUsers;
  }

  getMessages(partnerKey) {
    this.messages = this.db.list('/users/'+this.afAuth.auth.currentUser.uid+'/chats/'+partnerKey) as FirebaseListObservable<any>;


    return this.messages;
  }

  sendMessage(partnerKey, messageValue, form) {
    let d = new Date();
    let message = {
      date: d.toString(),
      from: true,
      message: messageValue
    }
    let messageToPartner = {
      date: d.toString(),
      from: false,
      message: messageValue
    }
    this.db.list('/users/'+this.afAuth.auth.currentUser.uid+'/chats/'+partnerKey).push(message);
    this.db.list('/users/'+partnerKey+'/chats/'+this.afAuth.auth.currentUser.uid).push(messageToPartner);

    form.reset();
  }

  joinChat() {
    this.UserService.getUser().subscribe(
      user => {
        if(user) {
          try {
            this.db.list('/chat/').update(user.$key, user)
              .then( test => console.log(test))
              .catch( error => {console.log(error)})
          }catch (error) {
            console.log(error);
          }


          console.log(user);
        }

      }
    )
  }

}
