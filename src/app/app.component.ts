import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { ChatService } from './services/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router,
    private afAuth: AngularFireAuth,
    private ChatService: ChatService
  ){
    this.afAuth.authState.subscribe(
      user => {
        if(!user && (this.router.url != 'login') && (this.router.url != 'register')) {
          this.router.navigate(['/login']);
        }
      },
      error => { console.log(error)}
    )
  }
}
