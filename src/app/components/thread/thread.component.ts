import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FirebaseService} from '../../services/firebase.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})
export class ThreadComponent implements OnInit {
  id: string;
  thread: any;
  answers: any;
  respondButton: boolean = false;
  user: any;
  subscription: any;
  subbed: boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private FirebaseService: FirebaseService,
    private UserService: UserService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    //Get Thread Info
    this.FirebaseService.getThread(this.id).subscribe(
      thread => {
        this.thread = thread;
        if(this.thread.answers) {
          this.answers = Object.keys(thread.answers).map(function (key) { return thread.answers[key]; });
        }
        this.UserService.getUserSubscription(this.thread.$key).subscribe(
          data => {
            this.subscription = data;
            if(data.$exists() == true) {
              this.subbed = true;
            }else {
              this.subbed = false;
            }
          },
          error => { console.log(error)}
        )
      },
      error => { console.log(error)}
    )
  }


  show() {
    console.log("this.subscription");
  }
}
