import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: boolean = false;
  email2: boolean = false;
  password: boolean = false;
  password2: boolean = false;
  name: boolean = false;
  vorname: boolean = false;
  email3: boolean = false;
  password3: boolean = false;
  password4: boolean = false;
  constructor(
    private UserService: UserService
  ) { }

  ngOnInit() {
  }

  registerAccount(form) {

    //Check for correct values
    if(!form.name){ this.name = true}
    if(!form.vorname){ this.vorname = true}
    if(!form.password){ this.password = true}
    if(!form.password2) {this.password2 = true}
    if(!form.email) {this.email = true}
    if(!form.email2) {this.email2 = true}
    if(form.email !== form.email2) {this.email3 = true}
    if(form.password !== form.password2){this.password3 = true}
    if(form.password.length < 6){this.password4 = true}


    //Check if values has  been corrected
    if(form.name){ this.name = false}
    if(form.vorname){ this.vorname = false}
    if(form.password){ this.password = false}
    if(form.password2) {this.password2 = false}
    if(form.email) {this.email = false}
    if(form.email2) {this.email2 = false}
    if(form.email == form.email2) {this.email3 = false}
    if(form.password == form.password2){this.password3 = false}
    if(form.password.length >= 6){this.password4 = false}

    //Register new User
    if(!this.name && !this.vorname && !this.email && !this.email2 && !this.email3 && !this.password && !this.password2 && !this.password4){
      //Create User Object for DB
      let d = new Date();
      let newUser = {
        name: form.name,
        vorname: form.vorname,
        registeredDate: d.toString(),
        isGoogle: false,
        chats: {},
        imgURL: null
      }

      //Register with Email and Password on firebase
      this.UserService.registerUser(newUser, form)
    }
    //TODO: Email already in user notification
  }

}
