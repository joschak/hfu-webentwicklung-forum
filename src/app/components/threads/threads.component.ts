import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';


@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.css']
})
export class ThreadsComponent implements OnInit {
  threads: any;
  newthreadbutton: boolean = false;
  constructor(
    private FirebaseService: FirebaseService
  ) { }

  ngOnInit() {
    this.FirebaseService.getThreads().subscribe(
      data => { this.threads = data },
      error => { console.log(error)}
    )
  }

}
