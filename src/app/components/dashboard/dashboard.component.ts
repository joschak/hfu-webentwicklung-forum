import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FirebaseService } from '../../services/firebase.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  recentSubbedComments: any;
  subscriptions: any;
  recentComments: any;
  recentThreads: any;
  allThreads: any;
  isempty: boolean;
  constructor(
    private UserService: UserService,
    private FirebaseService: FirebaseService,
    private router: Router,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.FirebaseService.getRecentThreads().subscribe(
      data => { this.recentThreads = data },
      error => { console.log(error)}
    )

    this.FirebaseService.getRecentComments().subscribe(
      data => { this.recentComments = data },
      error => { console.log(error)}
    )

    this.UserService.getUserSubscriptions().subscribe(
      data => { this.subscriptions = data },
      error => { console.log(error)}
    )

    this.FirebaseService.getThreads().subscribe(
      data => { this.allThreads = data },
      error => { console.log(error)}
    )

    this.afAuth.authState.subscribe(
      user => {
        if(!user) {
          this.router.navigate(['/login']);
        }
      }
    )
  }

}
