import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(
    private afAuth: AngularFireAuth,
    private UserService: UserService,
    private router: Router
  ) {
    this.afAuth.authState.subscribe(
      user => { if(!user && this.router.url !== 'login' && this.router.url !== 'register') {
        this.router.navigate(['/login'])}},
      error => { console.log(error)}
    )
  }

  ngOnInit() {

  }


}
