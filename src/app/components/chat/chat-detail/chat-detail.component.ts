import { Component, OnInit, Input } from '@angular/core';
import { ChatService } from '../../../services/chat.service';
declare var $:any;

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html',
  styleUrls: ['./chat-detail.component.css']
})
export class ChatDetailComponent implements OnInit {
  @Input() chatPartner: any;
  messages: any;

  constructor(
    private ChatService: ChatService
  ) { }

  ngOnInit() {
    $('#chatHistory').animate({
      scrollTop: $('#chatHistory').get(0).scrollHeight}, 200);
  }

  ngOnChanges() {
    if(this.chatPartner) {
      this.ChatService.getMessages(this.chatPartner.$key).subscribe(
        data => {
          this.messages = data;
          $('#chatHistory').animate({
            scrollTop: $('#chatHistory').get(0).scrollHeight}, 200);
        },
        error => { console.log(error)}
      )
    }

  }

}
