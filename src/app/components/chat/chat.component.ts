import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  onlineUsers: any;
  user: any;
  chatClosed: boolean = true;
  chatPartner: any = null;
  constructor(
    private UserService: UserService,
    private ChatService: ChatService
  ) { }

  ngOnInit() {
    this.UserService.getUser().subscribe(
      user => {
        this.user = user
        this.ChatService.getOnlineUsers().subscribe(
          data => { this.onlineUsers = data },
          error => { console.log(error) }
        )
      },
      error => { console.log(error) }
    )

  }


  selectChatPartner(partner) {
    console.log(partner);
  }

}
