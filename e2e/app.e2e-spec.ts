import { FinalForumPage } from './app.po';

describe('final-forum App', () => {
  let page: FinalForumPage;

  beforeEach(() => {
    page = new FinalForumPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
